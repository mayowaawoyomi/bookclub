# Korapay Book Club web application

This is a dashboard view of the web application built with HTML, SCSS, and Vanilla JavaScript.

## Features

- Responsive design for various screen sizes.
- The search box with an autocomplete dropdown.
- The Featured Books carousel and its book items 


## Installation

1. Clone this repository to your local machine:

   ```bash
   git clone https://github.com/mayowasam/bookclub.git


2. Navigate to the project folder:

cd bookclub

3. Install the required dependencies using npm:

npm install

## Usage

1. npm run dev

2. open  the index.html with a browser to use Korapay Book Club web application

## Development

- index.html contains the HTML structure of the Book Club web application.
- main.scss in the styles folder located in src contains the SCSS code for styling.
- script.js in the script folder located in src contains the JavaScript code for book club logic.
- data.js which contains dummy data queried 

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
