
const sidebarContainer = document.querySelector('.sidebar')
const sidebarContentContainer = document.querySelector('.sidebar-content')
const sidebarBackButton = document.querySelector('.sidebar-back-button')
const menuButton = document.querySelector('.hamburger')
const overlay = document.querySelector('.sidebar-overlay');


const destopSidebar = document.querySelector('.content-sidebar')
const bookcontainers = document.querySelectorAll('.books-section-container')
const featureContainer = document.querySelector('.carousel');

const searchIcon = document.querySelector('.search-mobile-icon');
const navbarSearch = document.querySelector('.navbar-mobile-search');
const navbarMenu = document.querySelector('.navbar-mobile-menu');
const navbarUserinfo = document.querySelector('.navbar-mobile-userinfo');
const backIcon = document.querySelector('.navbar-mobile-back')

const input = document.getElementById('myInput');
const resultsContainer = document.getElementById('autocompleteResults');

const mobile = document.querySelector('.mobile');
const mobileInput = document.getElementById('mobile-input');
const mobileResultsContainer = document.getElementById('mobile-autocompleteResults');


function toggleSidebar() {
    overlay.style.width = '100%';
    overlay.style.opacity = '1';
    sidebarContainer.style.left = '0';
}

function closeSidebar() {
    overlay.style.width = '0';
    overlay.style.opacity = '0';
    sidebarContainer.style.left = '-85%';
}

// Function to handle window resize
function handleWindowResize() {
    const windowWidth = window.innerWidth;

    if (windowWidth >= 768) {
        // Close the overlay and sidebar when the window width is greater than or equal to 768 (desktop view)
        closeSidebar();
    }
}

// Add event listener for window resize
window.addEventListener('resize', handleWindowResize);



//open sidebar in mobile view
menuButton.addEventListener('click', toggleSidebar)

//close sidebar in mobile view
overlay.addEventListener('click', closeSidebar);

//close sidebar in mobile view
sidebarBackButton.addEventListener('click', closeSidebar)



// Populate the mobile sidebar
sidebar.forEach((item, i) => {
    const div = document.createElement('div');
    div.className = 'sidebar-item';

    div.innerHTML = `
    <p class="${i === 0 ? "active" : ""}">${item.title}</p>
    <ul>
        ${item.items.map((value) => `
            <li>
            <span>${value.name}</span>
            ${value.value !== undefined ? `<span class="${value.active ? 'green' : ''} itemnumber">${value.value}</span>` : ''}
        </li>
               
        `).join('')}
    </ul>
    `;

    sidebarContentContainer.appendChild(div); // Append the div to the container
})


// Populate the desktop sidebar
sidebar.forEach((item, i) => {
    const div = document.createElement('div');
    div.className = 'sidebar-item';

    div.innerHTML = `
    <p class="${i === 0 ? "active" : ""}">${item.title}</p>
    <ul>
        ${item.items.map((value) => `
            <li>
            <span>${value.name}</span>
            ${value.value !== undefined ? `<span class="${value.active ? 'green' : ''} itemnumber">${value.value}</span>` : ''}
        </li>
               
        `).join('')}
    </ul>
    `;

    destopSidebar.appendChild(div); // Append the div to the container
})

//create more books
const newbooks = [];
while (newbooks.length < 9) {
    newbooks.push(...books);
}


//populate featured books
newbooks.forEach((item, i) => {
    const article = document.createElement('article');
    article.className = 'carousel-cell';

    let tag = item.tags.map(tagItem => tagItem).join(" , ");
    const status = item.status === "Available" ? 'green' : 'red';

    article.innerHTML = `

        <div class="featured-image"> 
            <img  src="${item.image}" alt=${item.name} width="220" height="300">  
            <button class="menuicon" onclick="toggleOverlay(${i})">            
                <img  src="/assets/details_toggle.svg" alt="menu icon" width="50" height="70"  >  
            </button>

            <div class="overlay" id="overlay-${i}" onclick="closeOverlay(${i})">
                <div class="book-info">
                    <span class="${status}">${item.status}</span>

                    <div class="book-name">
                        <strong>${item.name}</strong>
                        <p>${item.author}</p>
                    </div>
                    <div class="book-type">
                        <p>Genre:<span> ${item.type}<span></p>
                        <p>Label:<span> ${tag} <span></p>
                    </div>

                    <div class="book-detail">

                        <div class="book-type">
                            <p>Ratings: <span>${item.rating}</span></p>
                            <img src="/assets/star.svg" alt="star rating" width="70" height="11"/>
                        </div>

                        <div class="divider"></div>

                        <div class="book-actions">

                            <div>
                                <img src="/assets/greypeople.svg" alt="people icon" width="20" height="11"/>
                                <p>${item.readers}</p>
                            </div>
                        
                            <div>
                                <img src="/assets/greylikes.svg" alt="like icon" width="15" height="11"/>
                                <p>${item.likes}</p>
                            </div>
                        </div>
                    
                    </div
                </div>     
            </div>
        </div> 
  `;

    featureContainer.appendChild(article);
});


//Show overlay in feature when menu button is clicked 
function toggleOverlay(index) {
    const overlay = document.getElementById(`overlay-${index}`);
    overlay.classList.toggle('show');
}

//Close overlay  in feature when menu button is clicked 
function closeOverlay(index) {
    const overlay = document.getElementById(`overlay-${index}`);
    overlay.classList.remove('show');
}


// Populate the recent and all books
bookcontainers.forEach(section => {
    const booksContainer = section.querySelector('.books-section-content');

    books.forEach(item => {
        const div = document.createElement('div');
        div.className = 'book-item';
        const status = item.status === "Available" ? 'green' : 'red';

        div.innerHTML = `
            <div class="book-image">
                <img src=${item.image} alt=${item.name} width="300" height="400"/> 
            </div>

            <div class="book-info">
                <span class="${status}">${item.status}</span>

                <div class="book-name">
                    <strong>${item.name}</strong>
                    <p>${item.author}</p>
                    <p>${item.type}</p>
                </div>

                <div class="book-detail">

                    <div>
                        <p>Ratings: ${item.rating}</p>
                        <img src="/assets/star.svg" alt="star rating" width="70" height="11"/>
                    </div>

                    <div class="divider"></div>

                    <div class="book-actions">

                        <div>
                            <img src="/assets/people.svg" alt="people icon" width="20" height="11"/>
                            <p>${item.readers}</p>
                        </div>
                    
                        <div>
                            <img src="/assets/likes.svg" alt="like icon" width="20" height="11">
                            <p>${item.likes}</p>
                        </div>
                    </div>
                
                </div>
                    
            </div>
    
      `;

        booksContainer.appendChild(div);
    });
});


// opening input in mobile 
searchIcon.addEventListener('click', () => {
    // Toggle visibility of elements
    navbarSearch.style.display = 'flex';
    navbarMenu.style.display = 'none';
    navbarUserinfo.style.display = 'none';
    mobile.classList.toggle('fullscreen');

});

// closing input in mobile 
backIcon.addEventListener('click', () => {
    // Toggle visibility of elements
    navbarSearch.style.display = 'none';
    navbarMenu.style.display = 'flex';
    navbarUserinfo.style.display = 'flex';
    mobile.classList.toggle('fullscreen');

});

// Autocomplete in desktop
input.addEventListener('focus', function () {
    resultsContainer.style.display = 'flex';

});

input.addEventListener('blur', function () {
    // Use a small delay before hiding to allow the click on autocomplete result
    setTimeout(() => {
        resultsContainer.style.display = 'none';
    }, 1000);
});


input.addEventListener('input', function () {
    const searchQuery = input.value.toLowerCase();

    // Clear previous results
    resultsContainer.innerHTML = '';

    // Filter books based on search query or return all books if searchQuery is empty
    const filteredBooks = searchQuery.trim() === ''
        ? books : books.filter(book => {
            const nameAndAuthor = `${book.name} - ${book.author}`;
            return nameAndAuthor.toLowerCase().includes(searchQuery);
        });


    const maxResults = Math.min(filteredBooks.length, 4);

    const isSingleItem = maxResults === 1;

    // Display matched books in results container
    filteredBooks.slice(0, maxResults).forEach((book, index) => {
        const resultItem = document.createElement('div');
        const isLastItem = index === maxResults - 1;
        resultItem.className = "filtereditem"


        resultItem.onclick = function () {
            input.value = book.name + " " + book.author; 
            displayFilteredBooks(book.name);
            resultsContainer.style.display = 'none'; // Hide the autocomplete results

        };


        // Determine classes for name and author
        const nameClass = isLastItem && !isSingleItem ? 'light' : 'dark';
        const authorClass = isLastItem && !isSingleItem ? 'light' : 'light'; // All authors are light except for the last item

        const shortenedAuthor = book.author.length > 29 ? `${book.author.slice(0, 29)}...` : book.author;

        resultItem.innerHTML = `
      <span class="${nameClass}">${book.name}</span> - <span class="${authorClass}">${shortenedAuthor}</span>
    `;
        resultItem.classList.add('autocomplete-result');
        resultsContainer.appendChild(resultItem);
    });


    if (maxResults === 1) {
        const singleResultItem = resultsContainer.querySelector('.filtereditem');
        singleResultItem.classList.remove('light');
        singleResultItem.classList.add('dark');
    }

    // Check if searchQuery is empty and reset the display if it is
    if (searchQuery.trim() === '') {
        displayFilteredBooks(''); // Display all books
    }
});



// Function to display filtered books
function displayFilteredBooks(selectedValue) {
    // Clear the current content of book containers
    bookcontainers.forEach(section => {
        const booksContainer = section.querySelector('.books-section-content');
        booksContainer.innerHTML = '';
    });

    const filteredBooks = selectedValue.trim() === ''
        ? books
        : books.filter(book => selectedValue.toLowerCase().includes(book.name.toLowerCase()) || selectedValue.toLowerCase().includes(book.author.toLowerCase()));

    // Display the filtered books in the book containers
    bookcontainers.forEach(section => {
        const booksContainer = section.querySelector('.books-section-content');

        filteredBooks.forEach(item => {
            const div = document.createElement('div');
            div.className = 'book-item';
            const status = item.status === "Available" ? 'green' : 'red';

            div.innerHTML = `
                <div class="book-image">
                    <img src=${item.image} alt=${item.name} width="200" height="300"/> 
                </div>

                <div class="book-info">
                    <span class="${status}">${item.status}</span>

                    <div class="book-name">
                        <strong>${item.name}</strong>
                        <p>${item.author}</p>
                        <p>${item.type}</p>
                    </div>

                    <div class="book-detail">

                        <div>
                            <p>Ratings: ${item.rating}</p>
                            <img src="/assets/star.svg" alt="star rating" width="70" height="11"/>
                        </div>

                        <div class="divider"></div>

                        <div class="book-actions">

                            <div>
                                <img src="/assets/people.svg" alt="people icon" width="20" height="11"/>
                                <p>${item.readers}</p>
                            </div>
                    
                            <div>
                                <img src="/assets/likes.svg" alt="like icon" width="20" height="11">
                                <p>${item.likes}</p>
                            </div>
                        </div>
                
                    </div>
                    
                </div>
            `;

            booksContainer.appendChild(div);
        });
    });
}



// Mobile Autocomplete

mobileInput.addEventListener('focus', function () {
    mobileResultsContainer.style.display = 'flex';

});

mobileInput.addEventListener('blur', function () {
    // Use a small delay before hiding to allow the click on autocomplete result
    setTimeout(() => {
        mobileResultsContainer.style.display = 'none';
    }, 1000);
});


mobileInput.addEventListener('input', function () {
    const searchQuery = mobileInput.value.toLowerCase();

    // Clear previous results
    mobileResultsContainer.innerHTML = '';

    // Filter books based on search query or return all books if searchQuery is empty
    const filteredBooks = searchQuery.trim() === ''
        ? books : books.filter(book => {
            const nameAndAuthor = `${book.name} - ${book.author}`;
            return nameAndAuthor.toLowerCase().includes(searchQuery);
        });

    const maxResults = Math.min(filteredBooks.length, 4);

    const isSingleItem = maxResults === 1;
    // Display matched books in results container
    filteredBooks.slice(0, maxResults).forEach((book, index) => {
        const resultItem = document.createElement('div');
        const isLastItem = index === maxResults - 1;
        resultItem.className = "mobilefiltereditem"



        resultItem.onclick = function () {

            // Set the input value
            mobileInput.value = book.name + " " + book.author; 
            // Trigger the displayFilteredBooks function
            displayFilteredBooks(book.name);
            mobileResultsContainer.style.display = 'none'; // Hide the autocomplete results

        };


        // Determine classes for name and author
        const nameClass = isLastItem && !isSingleItem ? 'light' : 'dark';
        const authorClass = isLastItem && !isSingleItem ? 'light' : 'light'; // All authors are light except for the last item

        const shortenedAuthor = book.author.length > 29 ? `${book.author.slice(0, 29)}...` : book.author;

        resultItem.innerHTML = `
      <span class="${nameClass}">${book.name}</span> - <span class="${authorClass}">${shortenedAuthor}</span>
    `;
        mobileResultsContainer.appendChild(resultItem);
    });


    if (maxResults === 1) {
        const singleResultItem = mobileResultsContainer.querySelector('.mobilefiltereditem');
        singleResultItem.classList.remove('light');
        singleResultItem.classList.add('dark');
    }

    if (searchQuery.trim() === '') {
        displayFilteredBooks(''); // Display all books
    }
});

