const books = [
    {
        id:5,
        name:"Big Magic",
        author:"Elizabeth Gilbert - 2014",
        type:"Motivational",
        image:"/assets/big_magic.webp",
        rating:"4.0",
        status:"Available",
        readers: 39,
        likes:29,
        tags:["Creative", "Self-help"]

    },
    {
        id:4,
        name:"Effective Python",
        author:"Diomidis Spinellis",
        type:"Motivational",
        image:"/assets/effective_python.webp",
        rating:"4.0",
        status:"Available",
        readers: 39,
        likes:29,
        tags:["Creative", "Self-help"]

    },
  
    {
        id:2,
        name:"Built To Last",
        author:"Jim Collins, Jerry I. Porras - 2001",
        type:"Business",
        image:"/assets/built_to_last.webp",
        rating:"4.0",
        status:"Borrowed Out",
        readers: 39,
        likes:29,
        tags:["Creative", "Self-help"]

    },
    {
        id:3,
        name:"The Lean Startup",
        author:"Eric Reis - 2005",
        type:"Motivational",
        image:"/assets/the_lean_startup.webp",
        rating:"4.0",
        status:"Available",
        readers: 39,
        likes:29,
        tags:["Creative", "Self-help"]

    }, 
    {
        id:1,
        name:"The Effective Engineer",
        author:"Edmond Lau - 2009",
        type:"Motivational",
        image:"/assets/effective_engineer.webp",
        rating:"4.0",
        status:"Available",
        readers: 39,
        likes:29,
        tags:["Creative", "Self-help"]
    },
  

]

const sidebar = [
    {
        id: 1,
        title:"Home",
        items:[
            {
                name: "Profile",
            },
            {
                name: "Notification",
                value: 3,
                active: true
            }
        ]
    },
    {
        id: 2,
        title:"Explore",
        items:[
            {
                name: "Books",
                value: 273
            },
            {
                name: "Genre",
                value: 42
            },
            {
                name: "Authors",
                value: 106
            }
        ]
    },
    {
        id: 3,
        title:"My Books",
        items:[
            {
                name: "Queued",
                value: 3
            },
            {
                name: "Currently Borrowed",
                value: 0,
            },
            {
                name: "Favourites",
                value: 19,
            },
            {
                name: "History",
            }
        ]
    },
    {
        id: 4,
        title:"Admin",
        items:[
            {
                name: "Books Request",
                value: 2,
                active: true
            },
            {
                name: "Members",
                value: 34,
            },
            {
                name: "Library Settings",
            }

        ]
    }
]


console.log(books.filter(book => book.name.includes('The Effective Engineer - Edmond Lau - 2009')));